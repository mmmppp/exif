# -*- coding: utf8 -*-
from exif import Image

import tkinter as tk
from tkinter import filedialog as fd
import tkinter.messagebox as mb


class MainMenu(tk.Tk):

    def __init__(self):
        super().__init__()
        '''!ОПРЕДЕЛИТЬСЯ С РАЗМЕРОМ И ПОЛОЖЕНИЕМ ОКНА!'''
        self.resizable(False, False)
        ww = self.winfo_screenwidth() // 2
        hh = self.winfo_screenheight() // 2
        w = self.winfo_screenwidth() // 4
        h = self.winfo_screenheight() // 4
        self.geometry('{}x{}+{}+{}'.format(ww, hh, w, h))
        self.wm_title('Мы обязательно придумаем название')
        self.config(bg="white")

        self.btn_file = tk.Button(self, text="Выбрать фото и\nпоказать метаданные", fg='black',
                                  command=lambda: MainMenu.make_it_beautiful(self))
        self.btn_file.place(relx=.5, rely=.4, anchor="c", height=40, width=220,
                            bordermode='outside')

    @staticmethod
    def get_exif():

        ''' !ПЕРЕПИСАТЬ ПОЛУЧЕНИЕ ФАЙЛА ЧЕРЕЗ WITH! '''
        filename = fd.askopenfilename(title="Открыть файл", initialdir="/")

        if filename != '':
            if filename.endswith(".jpg"):
                '''Заменить селф фото на что-то другое'''
                with open("{}".format(filename), "rb") as ph:
                    photo = Image(ph)
                if photo.has_exif:
                    tags = list(photo.list_all())
                    # наверное, data_tags надо как-то покрасивее получать
                    data_tags = []
                    for i in range(len(tags)):
                        data_tags.append(photo.get(tags[i]))

                    return tags, data_tags
                else:
                    msg = u'    Метаданные уже очищены\n    Попробуйте другое фото'
                    mb.showinfo("Инфо", msg)
                    return None, None
            else:
                msg = u'    Неккоректный тип файла\n    Попробуйте еще раз'
                mb.showwarning("Предупреждение", msg)
                return None, None
        else:
            return None, None

    # def clean_and_save(self):

    # new_file = fd.asksaveasfile(title="Сохранить файл", defaultextension=".jpg", )
    # if new_file:
    #    up= self.photo.delete_all()
    #     new_file.close()

    def make_it_beautiful(self):

        tags, data_tags = self.get_exif()

        if tags is not None and data_tags is not None:
            self.btn_file.destroy()
            tk.Label(self, text='Ваши метаданные', justify='center').place(relx=.1, rely=.1, anchor="w", height=40,
                                                                           width=220, bordermode='outside')
            text_tags = tk.Text()
            text_tags.place(relx=.1, rely=.4, anchor="w", height=200, width=500, bordermode='outside')
            # text_data = tk.Text()
            # text_data.place(relx=.5, rely=.4, anchor="w", height=200, width=250, bordermode='outside')

            '''Вставлять в первый что-то вроде вставка и.0 тег / вставка и.лен(тег[и]+5 дата_тег)'''

            for i in range(len(tags)):
                # print(len("                                                              "))
                if tags[i] != 'flash':

                # text_tags.insert('{}.{}'.format(i, 0), "help")
                # text_tags.insert('{}.{}'.format(i, 0), "help"+ ' '*100 + "me"+'\n')
                # loc = '{}.0'.format(i)
                # full_exif = [[tags[i], data_tags[i]] for i in range(len(tags))]
                # insert(stroka.stolbech)

                    text_tags.insert('{}.0'.format(i), tags[i] + ' '*(62-len(tags[i])-len(str(data_tags[i]))) + str(data_tags[i]) + '\n')
                # text_tags.insert('{}.0'.format(i+1), tags[i])
                #
                # text_tags.insert('{}.{}'.format(i+1, 500-int(len(data_tags[i])-2)), "1"+'\n')
                # loc = len(tags[i])*2
                # print(loc)

                # text_tags.insert('{}.{}'.format(i, loc), str(data_tags[i]) + '\n')

            # for i in range(len(tags)):
            #     loc = '{}.0'.format(i)
            #     text_data.insert(loc, str(data_tags[i]) + '\n')
            text_tags.configure(state='disabled')


if __name__ == "__main__":
    app = MainMenu()
    app.mainloop()
